class AddAttachmentLapinPubToBooks < ActiveRecord::Migration
  def self.up
    change_table :books do |t|
      t.attachment :lapin_pub
    end
  end

  def self.down
    remove_attachment :books, :lapin_pub
  end
end
