class AddAttachmentImgToEpisodes < ActiveRecord::Migration
  def self.up
    change_table :episodes do |t|
      t.attachment :episode_img
    end
  end

  def self.down
    remove_attachment :episodes, :episode_img
  end
end
