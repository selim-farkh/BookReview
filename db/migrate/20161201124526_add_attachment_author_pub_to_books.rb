class AddAttachmentAuthorPubToBooks < ActiveRecord::Migration
  def self.up
    change_table :books do |t|
      t.attachment :author_pub
    end
  end

  def self.down
    remove_attachment :books, :author_pub
  end
end
