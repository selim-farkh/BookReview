class AddEpisodeIdToStory < ActiveRecord::Migration[5.0]
  def change
     add_column :episodes, :story_id, :integer
  end
end
