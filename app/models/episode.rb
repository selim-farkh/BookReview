class Episode < ApplicationRecord
  belongs_to :story

  has_attached_file :episode_img, :styles => { :episode_index => "250x350>", :episode_show => "325x475>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :episode_img, :content_type => /\Aimage\/.*\z/
end
