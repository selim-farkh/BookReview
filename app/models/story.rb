class Story < ApplicationRecord
 belongs_to :book
 has_many :episodes

 has_attached_file :desc_img, :styles => { :desc_index => "250x350>", :desc_show => "325x475>" }, :default_url => "/images/:style/missing.png"
 validates_attachment_content_type :desc_img, :content_type => /\Aimage\/.*\z/
end
