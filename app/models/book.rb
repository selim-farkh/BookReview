class Book < ActiveRecord::Base
	belongs_to :user
	belongs_to :category
	has_many :reviews
	has_many :stories

	has_attached_file :book_img, :styles => { :book_index => "250x350>", :book_show => "325x475>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :book_img, :content_type => /\Aimage\/.*\z/

	has_attached_file :author_pub, :styles => { :author_index => "250x350>", :author_show => "325x475>" }, :default_url => "/images/:style/missing.png"
	validates_attachment_content_type :author_pub, :content_type => /\Aimage\/.*\z/

	has_attached_file :lapin_pub, :styles => { :lapin_index => "250x350>", :lapin_show => "325x475>" }, :default_url => "/images/:style/missing.png"
	validates_attachment_content_type :lapin_pub, :content_type => /\Aimage\/.*\z/
end
