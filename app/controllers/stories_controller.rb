class StoriesController < ApplicationController
  before_action :set_story, only: [:show, :edit, :update, :destroy]
  before_action :set_book, expect: [:destroy]

  # GET /stories
  # GET /stories.json
  def index
    @stories = Story.all
    @stories = Story.where(book: params[:book_id])
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
   @story = Story.find(params[:id])
  end

  # GET /stories/new
  def new
    @story = Story.new
  end

  # GET /stories/1/edit
  def edit
    @story = Story.find(params[:id])
  end

  # POST /stories
  # POST /stories.json
  def create
    @story = Story.new(story_params)
    @story.book = @book

    respond_to do |format|
      if @story.save!
        format.html { redirect_to @story, notice: 'L\'histoire a bien été crée.' }
        format.json { render :show, status: :created, location: @story }
      else
        format.html { render :new }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @story, notice: 'L\'histoire a bien été mise a jour.' }
        format.json { render :show, status: :ok, location: @story }
      else
        format.html { render :edit }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @story.destroy!
    respond_to do |format|
      format.html { redirect_to stories_index_url, notice: 'L\'histoire a bien été suprimée.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_story
       @story = Story.find(params[:id])
    end

    def set_book
      @book = Book.find(params[:book_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def story_params
      params.require(:story).permit(:title, :description, :episode_img, :book_id)
    end
end
