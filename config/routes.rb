Rails.application.routes.draw do
  resources :categories
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :books do
    resources :stories
    resources :reviews
  end
  resources :stories do
    resources :episodes
  end
  root 'books#index'
end
